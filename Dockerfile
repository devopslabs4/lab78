FROM node:18-alpine as builder

WORKDIR /react

COPY package.json ./
RUN yarn

COPY . ./
RUN yarn build

FROM nginx:1.12-alpine
COPY --from=builder /react/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
